module "name" {
  source = "git::https://gitlab.com/terraform-modules1582413/name.git"

  prefix      = var.project_prefix
  environment = var.project_environment
  resource    = "postgres-${var.postgres_custom_name}"
  name        = "${var.postgres_custom_name}-pgd"
}
