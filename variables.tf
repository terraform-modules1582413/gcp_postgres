variable "project_prefix" {
  type = string
}
variable "project_environment" {
  type = string
}
variable "project_name" {
  type = string
}

variable "gcp_project_id" {
  type = string
}
variable "gcp_network_project_id" {
  type = string
}
variable "gcp_region" {
  type = string
}
variable "gcp_network_id" {
  type = string
}

variable "postgres_custom_name" {
  type    = string
  default = "db"
}
variable "postgres_deletion_protection" {
  type    = bool
  default = true
}
variable "postgres_version" {
  type    = string
  default = "POSTGRES_14"
}
variable "postgres_tier" {
  type    = string
  default = "db-f1-micro"
}
variable "postgres_availability_type" {
  type    = string
  default = "ZONAL"
}
variable "postgres_deletion_protection_enabled" {
  type    = bool
  default = true
}
variable "postgres_disk_autoresize" {
  type    = bool
  default = true
}
variable "postgres_disk_autoresize_limit" {
  type    = number
  default = 11
}
variable "postgres_disk_size_gb" {
  type    = number
  default = 10
}
variable "postgres_disk_type" {
  type    = string
  default = "PD_HDD"
}
variable "postgres_backup_enabled" {
  type    = bool
  default = true
}
variable "postgres_backup_start_time_hh_mm" {
  type    = string
  default = "03:00"
}
variable "postgres_backup_point_in_time_recovery_enabled" {
  type    = bool
  default = false
}
variable "postgres_backup_location" {
  type    = string
  default = "europe-central2"
}
variable "postgres_backup_transaction_log_retention_days_1_7" {
  type    = string
  default = "1"
}
variable "postgres_backup_retained_backups" {
  type    = string
  default = "3"
}
variable "postgres_maintenance_window_day" {
  type    = number
  default = 2
}
variable "postgres_maintenance_window_hour" {
  type    = number
  default = 2
}
variable "postgres_maintenance_window_update_track" {
  type    = string
  default = "canary"
}