<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_cloudflare"></a> [cloudflare](#requirement\_cloudflare) | ~> 4.0.0 |
| <a name="requirement_google"></a> [google](#requirement\_google) | ~> 4.59.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | ~> 4.59.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_name"></a> [name](#module\_name) | git::https://gitlab.com/terraform-modules1582413/name.git | n/a |

## Resources

| Name | Type |
|------|------|
| [google_sql_database_instance.main](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/sql_database_instance) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_gcp_network_id"></a> [gcp\_network\_id](#input\_gcp\_network\_id) | n/a | `string` | n/a | yes |
| <a name="input_gcp_network_project_id"></a> [gcp\_network\_project\_id](#input\_gcp\_network\_project\_id) | n/a | `string` | n/a | yes |
| <a name="input_gcp_project_id"></a> [gcp\_project\_id](#input\_gcp\_project\_id) | n/a | `string` | n/a | yes |
| <a name="input_gcp_region"></a> [gcp\_region](#input\_gcp\_region) | n/a | `string` | n/a | yes |
| <a name="input_postgres_availability_type"></a> [postgres\_availability\_type](#input\_postgres\_availability\_type) | n/a | `string` | `"ZONAL"` | no |
| <a name="input_postgres_backup_enabled"></a> [postgres\_backup\_enabled](#input\_postgres\_backup\_enabled) | n/a | `bool` | `true` | no |
| <a name="input_postgres_backup_location"></a> [postgres\_backup\_location](#input\_postgres\_backup\_location) | n/a | `string` | `"europe-central2"` | no |
| <a name="input_postgres_backup_point_in_time_recovery_enabled"></a> [postgres\_backup\_point\_in\_time\_recovery\_enabled](#input\_postgres\_backup\_point\_in\_time\_recovery\_enabled) | n/a | `bool` | `false` | no |
| <a name="input_postgres_backup_retained_backups"></a> [postgres\_backup\_retained\_backups](#input\_postgres\_backup\_retained\_backups) | n/a | `string` | `"3"` | no |
| <a name="input_postgres_backup_start_time_hh_mm"></a> [postgres\_backup\_start\_time\_hh\_mm](#input\_postgres\_backup\_start\_time\_hh\_mm) | n/a | `string` | `"03:00"` | no |
| <a name="input_postgres_backup_transaction_log_retention_days_1_7"></a> [postgres\_backup\_transaction\_log\_retention\_days\_1\_7](#input\_postgres\_backup\_transaction\_log\_retention\_days\_1\_7) | n/a | `string` | `"1"` | no |
| <a name="input_postgres_custom_name"></a> [postgres\_custom\_name](#input\_postgres\_custom\_name) | n/a | `string` | `"db"` | no |
| <a name="input_postgres_deletion_protection"></a> [postgres\_deletion\_protection](#input\_postgres\_deletion\_protection) | n/a | `bool` | `true` | no |
| <a name="input_postgres_deletion_protection_enabled"></a> [postgres\_deletion\_protection\_enabled](#input\_postgres\_deletion\_protection\_enabled) | n/a | `bool` | `true` | no |
| <a name="input_postgres_disk_autoresize"></a> [postgres\_disk\_autoresize](#input\_postgres\_disk\_autoresize) | n/a | `bool` | `true` | no |
| <a name="input_postgres_disk_autoresize_limit"></a> [postgres\_disk\_autoresize\_limit](#input\_postgres\_disk\_autoresize\_limit) | n/a | `number` | `11` | no |
| <a name="input_postgres_disk_size_gb"></a> [postgres\_disk\_size\_gb](#input\_postgres\_disk\_size\_gb) | n/a | `number` | `10` | no |
| <a name="input_postgres_disk_type"></a> [postgres\_disk\_type](#input\_postgres\_disk\_type) | n/a | `string` | `"PD_HDD"` | no |
| <a name="input_postgres_maintenance_window_day"></a> [postgres\_maintenance\_window\_day](#input\_postgres\_maintenance\_window\_day) | n/a | `number` | `2` | no |
| <a name="input_postgres_maintenance_window_hour"></a> [postgres\_maintenance\_window\_hour](#input\_postgres\_maintenance\_window\_hour) | n/a | `number` | `2` | no |
| <a name="input_postgres_maintenance_window_update_track"></a> [postgres\_maintenance\_window\_update\_track](#input\_postgres\_maintenance\_window\_update\_track) | n/a | `string` | `"canary"` | no |
| <a name="input_postgres_tier"></a> [postgres\_tier](#input\_postgres\_tier) | n/a | `string` | `"db-f1-micro"` | no |
| <a name="input_postgres_version"></a> [postgres\_version](#input\_postgres\_version) | n/a | `string` | `"POSTGRES_14"` | no |
| <a name="input_project_environment"></a> [project\_environment](#input\_project\_environment) | n/a | `string` | n/a | yes |
| <a name="input_project_name"></a> [project\_name](#input\_project\_name) | n/a | `string` | n/a | yes |
| <a name="input_project_prefix"></a> [project\_prefix](#input\_project\_prefix) | n/a | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->