terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.59.0"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 4.0.0"
    }
  }
}
