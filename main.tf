resource "google_sql_database_instance" "main" {
  project          = var.gcp_project_id
  name             = module.name.full
  region           = var.gcp_region
  database_version = var.postgres_version

  deletion_protection = var.postgres_deletion_protection

  settings {
    tier                        = var.postgres_tier
    availability_type           = var.postgres_availability_type
    deletion_protection_enabled = var.postgres_deletion_protection_enabled
    disk_autoresize             = var.postgres_disk_autoresize
    disk_autoresize_limit       = var.postgres_disk_autoresize_limit
    disk_size                   = var.postgres_disk_size_gb
    disk_type                   = var.postgres_disk_type
    user_labels = {
      "environment" = var.project_environment
      "project"     = var.project_name
      "name"        = var.postgres_custom_name
    }

    backup_configuration {
      enabled                        = var.postgres_backup_enabled
      start_time                     = var.postgres_backup_start_time_hh_mm
      point_in_time_recovery_enabled = var.postgres_backup_point_in_time_recovery_enabled
      location                       = var.postgres_backup_location
      transaction_log_retention_days = var.postgres_backup_transaction_log_retention_days_1_7
      backup_retention_settings {
        retained_backups = var.postgres_backup_retained_backups
        retention_unit   = "COUNT"
      }
    }

    maintenance_window {
      day          = var.postgres_maintenance_window_day
      hour         = var.postgres_maintenance_window_hour
      update_track = var.postgres_maintenance_window_update_track
    }

    ip_configuration {
      ipv4_enabled                                  = false
      private_network                               = var.gcp_network_id
      enable_private_path_for_google_cloud_services = true
    }
  }
}